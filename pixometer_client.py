#! /usr/bin/env python
# -*- coding: utf8 -*-

"""pixometer commandline client

This client allows basic access to the pixometer REST-API. The code serves as 
an example for REST-API usage. Feel free to use it as a whole or in parts to 
implement your own client.

All source code and inlined documentation are provided under the following 
license (compare http://opensource.org/licenses/mit-license.html):


The MIT License (MIT)
    
Copyright (c) 2014 pixolus GmbH, Germany

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

import sys
import os.path
import logging
import json
import requests

from urlparse import urljoin, urlsplit

try:
    import http.client as http_client
except ImportError:
    # Python 2
    import httplib as http_client
http_client.HTTPConnection.debuglevel = 1




class PixometerError(IOError):
    """exceptions raised for errors reported by the pixometer REST-API"""

    def __init__ (self, errno, strerror):
        super(IOError, self).__init__(errno, strerror)


class PixometerClient(object):
    """A simple client for the pixometer REST-API"""
    
    
    class UrlHelper (dict):
        """allow for ease URL addressing"""
        
        def __init__(self, base_url, api=''):
            self.base_url = ('/'.join((base_url.rstrip('/'), api.strip('/')))).rstrip('/') + '/'
            self["get_token"] = "api-token-auth"
        
        def __getattr__(self, name):
            name = self.get(name, name)
            return urljoin(self.base_url, "%s/" % name)
    
    
    DEFAULT_PAGE_SIZE   = 500
    DEFAULT_API_VERSION = 'api/v1'
    DEFAULT_MAX_PAGES   = 1000
    
    
    def __init__(self, auth_token=None, base_url=None, accept_language=None, ca_bundle=None):
        self.params    = {'page_size': str(self.DEFAULT_PAGE_SIZE)}
        self.urls = self.UrlHelper (base_url, self.DEFAULT_API_VERSION)
        self.session = requests.Session()
        self.session.headers.update({
            'Content-Type': 'application/json',
            'Accept-Encoding': 'gzip,deflate',
            'Accept': 'application/json',
            'Accept-Language': accept_language,
        })
        if auth_token:
            self.session.headers.update({'Authorization': 'Token %s' % auth_token})
        if ca_bundle != None:
            self.session.verify = ca_bundle
        else:
            self.session.verify = True
    

    def raise_for_status(self, response):
        """raise an exeption if HTTP status code of response is not ok"""
            
        # do nothing if we get an HTTP status code indicating success
        if response.status_code == requests.codes.ok:
            return

        # see if we find specific pixolus error messages
        headers = response.headers
        if 'X-Pixolus-Error' in headers and 'X-Pixolus-Error-Msg' in headers:
            raise PixometerError(int(headers['X-Pixolus-Error']),
                                 headers['X-Pixolus-Error-Msg'])
        
        # as a last resort, ask 'requests' module to raise exceptions if needed
        response.raise_for_status()
        
        
    def create_user(self, email, password):
        """create new user and return authentication token"""
        
        return self.POST(self.urls.users,
                         {"email": email,
                         "password": password})["token"]


    def get_auth_token(self, email, password):
        """get authentication token for email and password"""
    
        return self.POST(self.urls.get_token,
                         {"email": email,
                          "password": password})["token"]


    def POST(self, url, json_data, params={}):
        """HTTP POST on API endpoint, sending and returning JSON data"""
        
        # create effective parameters by updating the defaults
        effective_params = self.params.copy()
        effective_params.update (params)
        
        # post and get the data
        logging.debug('POST to %s' % url)
        response = self.session.post(url, data=json.dumps(json_data))
        
        # handle errors
        self.raise_for_status(response)
        
        # done
        return response.json()
    
    
    def GET(self, url, params=None):
        """HTTP GET on API endpoint, returning JSON data"""
        
        if params is None:
            params = {}
        
        # create effective parameters by updating the defaults
        effective_params = self.params.copy()
        effective_params.update (params)
        
        # get the data
        logging.debug('GET to %s' % url)
        response = self.session.get(url, params=effective_params)
        
        # handle errors
        self.raise_for_status(response)
        
        # done
        return response.json()
    
    
    def get_list(self, url, params=None):
        """get all data from an endpoint
        
        The pixolus REST-API uses pagination for list endpoints, potentially
        requiring multiple GET operations to retrieve all list entries. This 
        method handles pagination and concatenates all results to a single list.
        """
        
        # ensure we have a dict that we can update with page numbers
        if params is None:
            params = {}
        elif params.has_key('page'):
            del params['page']
        
        # get first page
        result_page = self.GET(url, params)
        results = result_page['results']
        logging.debug('page size: ' + str(len(result_page['results'])))
        
        # if available, visit next pages
        for page in range(2, self.DEFAULT_MAX_PAGES + 1):
            if (not result_page.has_key('next')) or (not result_page['next']):
                break
            
            # now get that next page
            params['page'] = str(page)
            result_page = self.GET(url, params)
            results.extend(result_page['results'])
            logging.debug('page size: ' + str(len(result_page['results'])))
        
        # done
        return results


# MARK: - commandline argument handling


# actions performed on the REST-API
ACTIONS = ['register', 'login', 'users', 'meters', 'readings', 'csv', 'xls', 'pixometer-csv']

# accepted physical medium types for filtering
PHYSICAL_MEDIUM = {
    'electricity': 'Strom',
    'gas':         'Gas',
    'water':       'Wasser',
    'generic':     'Sonstige'}


def parse_commandline_arguments(logger = None):
    """Setup commandline arguments and return argparse 'args' object"""

    import argparse

    # try to modify the argparse default arguments so that we get a nicer
    # output by shifting the help texts some columns to the right
    class IndentedFormatter (argparse.RawDescriptionHelpFormatter):
        def __init__(self, **kwargs):
            kwargs['max_help_position'] = 40
            argparse.RawDescriptionHelpFormatter.__init__(self, **kwargs)
    parser = argparse.ArgumentParser(formatter_class=IndentedFormatter, description=__doc__, fromfile_prefix_chars='@')

    # filtering options
    parser.add_argument('--created-before',
                                        help='get only readings created before given timestamp')
    parser.add_argument('--created-after', 
                                        help='get only readings created after given timestamp')
    parser.add_argument('--modified-before', 
                                        help='get only readings modified before given timestamp')
    parser.add_argument('--modified-after', 
                                        help='get only readings modified after given timestamp')
    parser.add_argument('--applying-person',
                                        help='get only readings from the given person')
    parser.add_argument('--physical-medium',
                                        choices=PHYSICAL_MEDIUM.keys(),
                                        help='get only readings from meters with given medium')
    parser.add_argument('--meter',
                                        help='get only readings from the meter with this id')
    parser.add_argument('--owner',
                                        help='get only readings from the meters owned by the given person')
    
    # other options
    parser.add_argument('--anonymize',  action='store_true', 
                        help='anonymize user names')
    parser.add_argument('--ca-bundle',  help='path to a CA Bundle with SSL certificate roots')
    parser.add_argument('--auth-token',
                        help='authentication token received from "login" action')
    parser.add_argument('--accept-language',
                        help='preferred client language, given as ISO code, e.g. en-US')
    parser.add_argument('--base-url',
                        default='https://pixometer.io',
                        help='base url, e.g. https://localhost:8080. Default: %(default)s')
    parser.add_argument('--number-before-street',    action='store_true',
                        help='assume English or French address notation with the house number preceding the street name')
    parser.add_argument('--verbose',    action='count',      help='print detailed output')
    
    # required
    parser.add_argument('action',       choices=ACTIONS)
    args = parser.parse_args()

    # configure logging
    logging.captureWarnings(True)
    
    # react on 'verbose' argument
    if logger and args.verbose > 0:
        logging.basicConfig(level=logging.INFO if args.verbose == 1 else logging.DEBUG, format='# %(message)s')
        
    # deactivate verbose logging for requests
    import httplib
    httplib.HTTPConnection.debuglevel = 0
    requests_log = logging.getLogger("requests.packages.urllib3")
    requests_log.propagate = False
    requests_log.setLevel(logging.ERROR)


    return args


def parse_timestamp(string):
    """ parse datetime string
    
    Helper function for handling command line arguments.
    A given string representing a timestamp with format YYYY-mm-dd or YYYY-MM-DDTHH:MM
    (e.g. 2014-08-26 or 2014-08-26T12:30) is converted into a string with format
    %Y-%m-%dT%H:%M:%S.%f (e.g. 2014-08-26T00:00:00.0 or 2014-08-26T12:30:00.0)  
    
    @param  string    string representing a timestamp
    @return formatted string in format %Y-%m-%dT%H:%M:%S.%f
    """
    
    import datetime
    try:
        timestamp = datetime.datetime.strptime(string, '%Y-%m-%dT%H:%M')
        
    except ValueError as e:
        timestamp = datetime.datetime.strptime(string, '%Y-%m-%d')
            
    return timestamp.strftime('%Y-%m-%dT%H:%M:%S.%f')


# MARK: - __main__


if __name__ == '__main__':
    
    # get arguments from command line
    args = parse_commandline_arguments(logging)
    
    # from program options create filter parameters
    filter_params = {}
    if args.created_before:
        filter_params['created_before']   = parse_timestamp(args.created_before)    
    if args.created_after:
        filter_params['created_after']    = parse_timestamp(args.created_after)
    if args.modified_before:
        filter_params['modified_before']  = parse_timestamp(args.modified_before)
    if args.modified_after:
        filter_params['modified_after']   = parse_timestamp(args.modified_after)
    if args.applying_person:
        filter_params['applying_person']  = args.applying_person
    if args.physical_medium:
        filter_params['physical_medium']  = args.physical_medium
    if args.meter:
        filter_params['meter'] = args.meter
    if args.owner:
        filter_params['owner'] = args.owner

    # act on the provided action
    try:
        
        logging.info ('creating pixometer client')
        client = PixometerClient(args.auth_token, args.base_url, args.accept_language, args.ca_bundle)
        
        
        if args.action == 'register' or args.action == 'login':
            
            # input user data
            import getpass
            email    = raw_input('Email: ')
            password = getpass.getpass('Password: ')

            # collect the token either by registering or login
            if args.action == 'register':
                auth_token = client.create_user(email, password)
            else:
                auth_token = client.get_auth_token(email, password)
            
            # print result
            print auth_token
        
        elif args.action == 'users':
            
            # obtain all data from the server
            users = client.get_list(client.urls.users, filter_params)
            
            # print results
            print '# id email'
            for user in users:
                user_id = user['url'].rstrip('/').split('/')[-1]
                print user_id, user['email']

        elif args.action == 'meters':
            
            # obtain all data from the server
            meters = client.get_list(client.urls.meters, filter_params)
            
            # print results
            print '# id physical_medium physical_unit integer_digits fraction_digits is_double_tariff owner'
            for meter in meters:
                meter_id = meter['url'].rstrip('/').split('/')[-1]
                print meter_id, meter['physical_medium'], meter['physical_unit'], meter['integer_digits'], meter['fraction_digits'], meter['is_double_tariff'], meter['owner']

        elif args.action == 'readings':
            
            # obtain all data from the server
            readings = client.get_list(client.urls.readings, filter_params)

            # print results
            print '# reading_date value value_second_tariff notes'
            for reading in readings:
            
                # extract notes from image metadata
                notes = []
                if reading.has_key('image_meta') and reading['image_meta'].has_key('annotations'):
                    for annotation in reading['image_meta']['annotations']:
                        if annotation['meaning'] == 'notes':
                            notes.append(annotation['text'])
                if reading.has_key('image_meta_second_tariff') and reading['image_meta_second_tariff'].has_key('annotations'):
                    for annotation in reading['image_meta_second_tariff']['annotations']:
                        if annotation['meaning'] == 'notes':
                            notes.append(annotation['text'])
            
                # print line for reading
                print reading['reading_date'], reading['value'], reading['value_second_tariff'], ', '.join(notes)

        elif args.action in ('csv', 'xls'):
            
            # we need some extra modules to make most use of Python
            import sys
            
            # obtain all data from the server
            logging.info('getting users from server')
            if args.applying_person:
                filter_params['email'] = args.applying_person
            elif args.owner:
                filter_params['email'] = args.owner
            users = client.get_list(client.urls.users, filter_params)
            logging.info('getting meters from server')
            meters = client.get_list(client.urls.meters, filter_params)
            logging.info('getting readings from server')
            readings = client.get_list(client.urls.readings, filter_params)
            
            # create lookup tables for user names and meter serial numbers
            # but anonymize user names by replacing email addresses with SHA1 hashes
            import hashlib
            email_address = {}
            for user in users:
                if args.anonymize:
                    email_address[user['url']] = hashlib.sha1(user['email']).hexdigest()
                else:
                    email_address[user['url']] = user['email']
            meter_details = {}
            for meter in meters:
                meter_details[meter['url']] = (meter['meter_id'], meter['location_in_building'])
            
            # print results
            logging.info('writing result to stdout')
            if (args.action == 'csv'):
                import csv
                
                writer = csv.writer(sys.stdout)
                writer.writerow(('date',
                                 'meter id',
                                 'location_in_building',
                                 'applying person',
                                 'value',
                                 'value (second tariff)',
                                 'notes'))
                for reading in readings:
                    
                    # extract notes from image metadata
                    notes = []
                    if reading.has_key('image_meta') and reading['image_meta'].has_key('annotations'):
                        for annotation in reading['image_meta']['annotations']:
                            if annotation['meaning'] == 'notes':
                                notes.append(annotation['text'])
                
                    # print single line
                    writer.writerow([s.encode("utf-8") for s in
                                     (reading['reading_date'],
                                     meter_details[reading['meter']][0],
                                     meter_details[reading['meter']][1],
                                     email_address[reading['applying_person']],
                                     reading['value'],
                                     reading['value_second_tariff'],
                                     ', '.join(notes))])
            else:
                import xlwt
                import dateutil.parser
                
                header_format   = xlwt.easyxf("font: bold on; alignment: horiz centre; borders: bottom medium")
                iso_date_format = xlwt.easyxf("", "yyyy-mm-dd hh:mm")
                float_3         = xlwt.easyxf("", "0.000")
                reading_format  = {}
                for meter in meters:
                    fraction_digits = int(meter['fraction_digits'])
                    physical_unit   = meter['physical_unit']
                    format_string = "0." + ("0" * fraction_digits) if fraction_digits > 0 else "0"
                    reading_format[meter['url']] = xlwt.easyxf("", format_string)
                
                document = xlwt.Workbook()
                sheet = document.add_sheet('Readings')
                for col, (column_header, column_width) in enumerate((('Date read',        20),
                                                                     ('Date uploaded',    20),
                                                                     ('Meter id',         20),
                                                                     ('Location in Building', 20),
                                                                     ('Applying person',  35),
                                                                     ('Applied method',   20),
                                                                     ('Main tariff',      15),
                                                                     ('Second tariff',    15),
                                                                     ('Main confidence',  15),
                                                                     ('Second confidence',15),
                                                                     ('Main sharpness',   15),
                                                                     ('Second sharpness', 15),
                                                                     ('Main OCR',         15),
                                                                     ('Second OCR',       15),
                                                                     ('Sec since start',  15),
                                                                     ('Sec since detection', 15),
                                                                     ('Frame number',     15),
                                                                     ('App version',      20),
                                                                     ('SDK version',      20),
                                                                     ('OS version',       20),
                                                                     ('Camera model',   35),
                                                                     ('Notes',          45))):
                    sheet.write(0, col, column_header, header_format)
                    sheet.col(col).set_width(256 * column_width)
                for row, reading in enumerate(readings):
                    # extract data from meta as well
                    main_meta   = reading['image_meta']
                    second_meta = reading['image_second_tariff_meta']
                    sdk_version = "unknown"
                    notes = []
                    reading_id  = str(reading['resource_id']) if reading.has_key('resource_id') else ""
                    
                    # extract meta data for first tariff
                    main_value      = None
                    main_confidence = None
                    main_sharpness  = None
                    if main_meta != None:
                        for annotation in main_meta['annotations']:
                            try:
                                if annotation['meaning'] == 'value':
                                    main_value      = annotation['text']
                                    main_confidence = annotation['rectangle']['confidence']
                                    main_confidence = float(main_confidence) if main_confidence != None else float(-1)
                                    main_sharpness  = annotation['rectangle']['sharpness']
                                    main_sharpness  = float(main_sharpness)  if main_sharpness  != None else float(-1)
                                elif annotation['meaning'] == 'com.pixolus.meterreading.version':
                                    sdk_version = annotation['text']
                                elif annotation['meaning'] == 'notes':
                                    notes.append(annotation['text'])
                            except:
                                logging.error("There was a problem extracting the following annotiation: " + str(annotation) + " for reading " + reading_id)

                    # extract meta data for second tariff
                    second_value      = None
                    second_confidence = None
                    second_sharpness  = None
                    if second_meta != None:
                        for annotation in second_meta['annotations']:
                            try:
                                if annotation['meaning'] == 'value':
                                    second_value      = annotation['text']
                                    second_confidence = annotation['rectangle']['confidence']
                                    second_confidence = float(second_confidence) if second_confidence != None else float(-1)
                                    second_sharpness  = annotation['rectangle']['sharpness']
                                    second_sharpness  = float(second_sharpness)  if second_sharpness  != None else float(-1)
                                elif annotation['meaning'] == 'notes':
                                    notes.append(annotation['text'])
                            except:
                                logging.error("There was a problem extracting the following annotiation: " + str(annotation) + " for reading " + reading_id)

                    # write whole row column by column
                    sheet.write(row+1,  0, dateutil.parser.parse(reading['reading_date'], ignoretz=True), iso_date_format)
                    sheet.write(row+1,  1, dateutil.parser.parse(reading['created'], ignoretz=True), iso_date_format)
                    sheet.write(row+1,  2, meter_details[reading['meter']][0])
                    sheet.write(row+1,  3, meter_details[reading['meter']][1])
                    sheet.write(row+1,  4, email_address[reading['applying_person']])
                    sheet.write(row+1,  5, reading['applied_method'])
                    sheet.write(row+1,  6, float(reading['value']),               reading_format[reading['meter']])
                    if reading['value_second_tariff'] != None:
                        sheet.write(row+1,  7, float(reading['value_second_tariff']), reading_format[reading['meter']])
                    if main_confidence != None:
                        sheet.write(row+1,  8, float(main_confidence),   float_3)
                    if second_confidence != None:
                        sheet.write(row+1,  9, float(second_confidence), float_3)
                    if main_sharpness != None:
                        sheet.write(row+1, 10, float(main_sharpness),    float_3)
                    if second_sharpness != None:
                        sheet.write(row+1, 11, float(second_sharpness),  float_3)
                    if main_value != None:
                        sheet.write(row+1, 12, main_value)
                    if second_value != None:
                        sheet.write(row+1, 13, second_value)
                    sheet.write(row+1, 14, float(main_meta['seconds_since_start']),      float_3)
                    sheet.write(row+1, 15, float(main_meta['seconds_since_detection']),  float_3)
                    sheet.write(row+1, 16, int(main_meta['frame_number']))
                    sheet.write(row+1, 17, main_meta['pixolus_version'])
                    sheet.write(row+1, 18, sdk_version)
                    sheet.write(row+1, 19, main_meta['os_version'])
                    sheet.write(row+1, 20, main_meta['camera_model'])
                    sheet.write(row+1, 21, ', '.join(notes))

                sheet.set_horz_split_pos(1)
                sheet.panes_frozen = True
                sheet.remove_splits = True
                document.save(sys.stdout)
                
        elif args.action == 'pixometer-csv':
            
            # obtain all data from the server
            meters = client.get_list(client.urls.meters, filter_params)
            
            # these are the columns of the new import template
            columns = [
                ('', u'Auftragskennung'),
                ('', u'Gebäudebezeichnung'),
                ('', u'Gebäudehinweis'),
                ('street', u'Straße'),
                ('number', u'Hausnummer'),
                ('zip_code', u'Postleitzahl'),
                ('city', u'Ort'),
                ('', u'Gebäudebetreuer'),
                ('', u'Ableser'),
                ('physical_medium', u'Energieart'),
                ('physical_unit', u'Einheit'),
                ('label', u'Zählerbezeichnung'),
                ('meter_id', u'Zählernummer'),
                ('', u'Barcode-ID'),
                ('', u'Zähler Status'),
                ('', u'Vorheriger Zähler'),
                ('', u'Hauptzähler'),
                ('', u'Messlokation'),
                ('', u'Marktlokation'),
                ('', u'Ablesehäufigkeit'),
                ('obis_code', u'OBIS'),
                ('appearance', u'Zählertyp'),
                ('integer_digits', u'Vorkommastellen'),
                ('fraction_digits', u'Nachkommastellen'),
                ('description', u'Zählerhinweis'),
                ('', u'Umwandlungsfaktor'),
                ('', u'Ablesehinweis'),
                ('', u'Ablesezeitraum Start'),
                ('', u'Ablesezeitraum Ende'),
                ('', u'minimaler Zählerstand'),
                ('', u'maximaler Zählerstand'),
                ('', u'Zählerstand'),
                ('', u'Ablesedatum'),
                ('', u'Status'),
                ('', u'Statusgrund'),
                ('', u'Bemerkungen'),
            ]
            
            import csv
            
            writer = csv.writer(sys.stdout)
            
            # print table header with column names
            writer.writerow([new_column.encode("utf-8") for (old_column, new_column) in columns])
            
            # print results
            for meter in meters:
                
                # emtpy field as placeholder for columns that were not present in the old pixometer
                meter[''] = ''
                
                # replace None values with empty strings
                for key, value in meter.items():
                    if value is None:
                        meter[key] = ''
                
                # split street name and number
                if args.number_before_street:
                    # English or French address notation, e.g. '1, rue des invalides'
                    address = meter['address'].split(',')
                    if len(address) == 1:
                        meter['street'] = address[0]
                        meter['number'] = ''
                    else:
                        meter['street'] = ','.join(address[1:])
                        meter['number'] = address[0]
                else:
                    # German address notation, e.g. 'Bundesplatz 1'
                    address = meter['address'].split(' ')
                    if len(address) == 1:
                        meter['street'] = address[0]
                        meter['number'] = ''
                    else:
                        meter['street'] = ' '.join(address[:-1])
                        meter['number'] = address[-1]
                
                # derive OBIS code list and update meter appearance
                if meter['physical_medium'] == 'electricity':
                    meter['physical_medium'] = u'Strom'
                    # meter['appearance'] = 'mechanical_black_or_lcd_edl21'
                    if meter['is_double_tariff']:
                        obis_codes = ['1-1:1.8.1', '1-1:1.8.2']
                    else:
                        obis_codes = ['1-1:1.8.0']
                elif meter['physical_medium'] == 'water':
                    meter['physical_medium'] = u'Wasser'
                    obis_codes = ['8-1:1.0.0']
                elif meter['physical_medium'] == 'gas':
                    meter['physical_medium'] = u'Gas'
                    obis_codes = ['7-1:3.0.0']
                elif meter['physical_medium'] == 'heat':
                    meter['physical_medium'] = u'Wärme'
                    obis_codes = ['6-1:1.8.0']
                else:
                    meter['physical_medium'] = u'Sonstige'
                    obis_codes = ['']
                    logging.warning("No suitable physical_medium for meter %s - setting it to generic",
                                    meter['meter_id'])
                
                # for every OBIS code / register, write a line
                for obis_code in obis_codes:
                    meter['obis_code'] = obis_code
                    writer.writerow(
                        [unicode(meter[old_column]).encode("utf-8") for (old_column, new_column) in columns])
    
        else:
            raise NotImplementedError ('This action has not been implemented yet')
    
    except PixometerError as error:
        logging.error('REST-API error %d: %s', error.errno, error.strerror)
        sys.exit(1)
    
    except requests.exceptions.RequestException as error:
        logging.error('Request error %s', error)
        sys.exit(1)

    except requests.exceptions.HTTPError as error:
        logging.error('HTTP error %s', error)
        sys.exit(1)

    # done
    logging.debug('exiting with status 0')
    sys.exit(0)
