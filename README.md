Overview
========

pixometer is a client-server-app, consisting of native mobile clients and a
server backend that runs on http://pixometer.io.

This documentation provides all details necessary to access the data on the
backend for integration with other web services or for writing custom clients.

*This documentation describes version 1 of the pixometer API.*

Table of Contents
-----------------

[TOC]

API basics
----------

All API endpoints try to adhere to [REST best practices][rest]. 
Although the API allows access via XML and JSON, clients should prefer JSON 
formatted requests as these are tested best. Opening the REST-API URLs within a 
web browser allows to see basic API description. Calls to the REST-API URLs can 
be made from the browser as well to test behaviour.

[rest]: http://www.restapitutorial.com

Clients specify all communications parameters using HTTP Headers:

-   `Content-Type`:
    Specifies the data format that the client sends the request in. Currently,
    only `application/json` and `multipart/form-data` are supported.
-   `Accept`:
    Specifies the data format that the client wishes to receive. Valid choices 
    are `application/json`, `application/xml` and `html`.
-   `Accept-Encoding`:
    Optional encoding of the contents. Valid choices are `gzip` and `deflate`.
    These allow compression of the results, resulting in reduced data traffic 
    and faster response times (typically).
-   `Accept-Language`:
    Given a valid ISO language specifier such as `en`,`fr`, or `de`, this header
    controls the language of returned text messages. Actual data contents is 
    never translated, but error messages or warnings will be using the given 
    localization. A list of ISO codes can be given, specifying the order of 
    preference. Defaults to `en`.


Error handling
==============

The backend server code itself performs checks for semantic integrity of the 
data and will respond with specific HTTP errors if data is not accepted. These 
error messages are described in this document alongside the respective API 
endpoints that can generate the error. In addition, the application server 
(which internally calls the pixometer server) can generate generic HTTP errors, 
for example when the pixometer server is overloaded or in an error state. These 
HTTP errors (indicated by HTTP status code `503`) are not described in this 
document as the current application server is provided by our PaaS provider.

Error levels
------------

Clients therefore might see these different levels of errors:

-   *Errors only visible to the client*
    Network connectivity problems or local network stack errors on the client 
    are the hardest to debug. These are never visible to the server and depend 
    on the specific client implementation and/or networking situation. Client 
    apps see these errors as error codes resulting from failed OS calls.
-   *Errors visible to client and application server*
    Network connectivity problems that occur during an established connection 
    are partly visible to the application server. Errors resulting from wrong 
    HTTP usage also are visible to the application server. Logging depends on 
    the application server settings at our PaaS - for production servers, these 
    are usually chosen less verbose for performance reasons. Clients see these 
    errors as HTTP status codes indicating errors or as error codes from failed 
    OS calls.
-   *Errors visible to client and backend code*
    If complete HTTP calls are received by the application server, the actual 
    backend server processes the requests. If errors occur here because of wrong 
    parameters, insufficient privileges, wrong requests, the pixometer backend 
    server code generates HTTP answers that consist of an HTTP status code, 
    custom HTTP headers `X-Pixolus-Error` and `X-Pixolus-Error-Msg` and an HTTP 
    body describing the error. Clients need to interpret HTTP status code and 
    the `X-Pixolus-Error` and `X-Pixolus-Error-Msg` headers (see below).

Custom HTTP headers
-------------------

Since some platforms have network stacks that do not provide HTTP bodies if HTTP
status codes from the `4xx` range appear, we chose to supply extra error 
information using two custom HTTP headers. 

These are used as follows:

-   `X-Pixolus-Error`: A numeric, integral value that allows for selecting a 
    specific response / behaviour of a client. Codes are documented alongside 
    every REST-API endpoint.
-   `X-Pixolus-Error-Msg`: Localized, human readable error description. This 
    description is meant to be used by client apps to present it to the user, 
    for example as a message body of an alert dialog. It is localized to the 
    language indicated by the originating HTTP request. Because of a set of 
    RFCs, the encoding is currently ISO-8859-1 (Latin-1) with some further 
    restrictions on whitespace and control characters. Simple clients can assume
    that this message is suitable for directly dumping it to log files. Future 
    extensions of this protocol might employ quoted printable encoding or 
    similar, but this has still to be defined.


User model, authentication, authorization and security
======================================================

Users are identified by an email address (as a username) and a password. 
Therefore, email addresses used with the service must be unique, i.e. there can 
be no two user accounts with the same email address. Passwords can be restricted
to certain criteria - currently only a minimum length of 8 characters is 
required.

Access privileges and roles
---------------------------

Users can have different access privileges / roles. Currently these are:

-   *Anonymous*: Without login in, only very few endpoints of the REST-API can 
    be accessed and functionality is mainly restricted to login and registration
    of new accounts.
-   *Standard User*: After successful login, a standard user has access to all 
    endpoints of the REST-API. Standard users can only access their own data.
-   *Admin User*: After successful login, an admin user has access to all 
    endpoints of the REST-API but can access data from all users of the server. 
    Admin users can create standard users and modify account details of standard 
    users.

User accounts can be active or inactive. When an anonymous user registers a new 
account, it is created in an inactive state. The server sends an email to the 
email address used as username which contains a special activation token. Using 
this token, the account can be switched to active state. Admin users can create 
user accounts that are active right from creation; admin users also can switch 
user accounts to active or inactive state.

Authentication
--------------

Through a web browser, access to REST-API URLs is authenticated with email 
address (as username) and password using [HTTP basic auth][basic]. Client apps, 
however, should only authenticate with the REST-API using [HTTP token auth][token].

[basic]: http://tools.ietf.org/html/rfc2617
[token]: http://tools.ietf.org/html/draft-hammer-http-token-auth-01

Authentication tokens can be requested using the Authentication Endpoint 
described below. An authentication token replaces username AND password and 
supplies the same access privileges as the username/password combination, 
including write/delete access to data. The only exception is password 
management, which always requires username and password. Future extensions of 
this protocol might specify additional tokens, limiting access to certain 
REST-API endpoints or limiting allowed actions on REST-API endpoints.

Transport security
------------------

All access must be using SSL/TLS encryption. The server emits a certificate 
for `pixometer.io`, that is signed by the pixolus certification agency. Client 
apps should show an error for a certificate that does not match the URL and 
must not allow access for non-matching certificates. Client apps are required to
only allow connections signed by the pixolus CA. This mechanism is needed to 
suppress man-in-the-middle attacks.

The pixolus CA certificate will be issued to client app developers via email.


Generic request modifiers
=========================

The API is built with the Django REST Framework and allows some built-in 
modifiers as well as specifically implemented modifiers to influence result 
sets:

-   *Page size*:
    
    All endpoints use built-in pagination. There is a default page size and a 
    maximum page size hardcoded into the server that an API user cannot / should 
    not find out directly. API users can only specify a *desired* page size. The
    server will (silently) cap it to the maximum page size. 
    
    Use URLs like:
    
    -   `https://pixometer.io/api/v1/readings/?page_size=3`
    
-   *Sorting*:
    
    You can sort by a specific entry and even reverse the sorting order of the 
    results. 

    Use URLs like this:
    
    -   `http://pixometer.io/api/v1/readings/?o=applying_person`
    -   `http://pixometer.io/api/v1/readings/?o=-created`
    
-   *Filtering*:
    
    You can filter results to show only a subset matching a specific entry. 
    
    Use URLs like this:
    
    -   `http://pixometer.io/api/v1/meters/?owner=someone@somewhere.org`
    -   `http://pixometer.io/api/v1/readings/?applying_person=someone@somewhere.org`
    -   `http://pixometer.io/api/v1/readings/?physical_medium=gas`
    -   `http://pixometer.io/api/v1/readings/?created_before=2015-06-01`
    -   `http://pixometer.io/api/v1/readings/?created_after=2015-06-01`
    -   `http://pixometer.io/api/v1/readings/?meter=1`
    -   `http://pixometer.io/api/v1/readings/?value=4711` (returns readings with values being greater than 4711)

Modifiers may be freely combined.


REST-API Endpoints
==================

Authentication Endpoint
-----------------------

Client apps should only authenticate using token auth. An access token can be 
obtained from the special endpoint `api-token-auth/` and is passed as additional
HTTP header `Authorization` with the value `Token XYZ` (with „XYZ“ replaced with
the actual token) for subsequent calls.

*Do not store the user passwords in your client app*, but instead ask a user for
the password once to obtain the token interactively. Then, instantly forget the 
password but save the token. You can also store the username to aid a user in 
re-obtaining a token at a later point in time.

Client apps can assume that the token remains valid for a long time (days or 
months). It can be invalidated upon request or as a result of security 
mechanisms to be defined in the future.


### Method: POST ###

-   Endpoint: `api-token-auth/`
-   Description: Authentication with credentials returns authentication token
-   Input: 
    -   `email`: email address of a registered user
    -   `password`: password of a registered user
-   Responses
    -   `200`: user name and password valid
    -   `400`: invalid credentials. 
        -   `X-Pixolus-Error=1`: user is inactive
        -   `X-Pixolus-Error=2`: email address was incorrect
        -   `X-Pixolus-Error=3`: password was incorrect
        -   `X-Pixolus-Error=8`: email address is missing
        -   `X-Pixolus-Error=9`: password is missing
-   Output:
	
		{	
			"email": "the_current_user@email.org",
	 	 	"token": "123abc54565423cd10d975416ca6801c563a5958",
	 	 	"url": "https://pixometer.io/api/v1/users/4711/",
	 	 	"user_id": 4711
	 	}

		-   `email`:    email address of this user
	    -   `token`:    authentication token
	    -   `url`:      url to this user at `/users/` endpoint
	    -   `user_id`:  unique, useable with `/users/` endpoint   
    

Access Token Endpoint
---------------------

Users may want to give third party services access to pixometer API without
sharing username/password. The Access Token Endpoint returns access tokens 
which can also have restricted access (read-only, write-only). Access tokens
expire after some time (currently 2 weeks). They can be invalidated upon request 
or as a result of security mechanisms to be defined in the future.
The token has to be passed as additional HTTP header `Authorization` with the
value `Bearer XYZ` (with „XYZ“ replaced with the actual token) for subsequent calls.

### Method: POST ###

-   Endpoint: `access-token/`
-   Description: Authentication with credentials returns access token
-   Input: 
    -   `email`: email address of a registered user
    -   `password`: password of a registered user
    -   `scope`: optional, restrict access to read-only or write-only 
    			 possible values: `read` or `write`. If not specified, 
    			 the returned token has read AND write access.   
-   Responses
    -   `200`: user name and password valid
    -   `400`: invalid credentials. 
        -   `X-Pixolus-Error=2`: email address was incorrect
        -   `X-Pixolus-Error=3`: password was incorrect
        -   `X-Pixolus-Error=8`: email address is missing
        -   `X-Pixolus-Error=9`: password is missing
        -   `X-Pixolus-Error=12`: given scope is invalid
-   Output:
	
		{
			"access_token": "12345abc123abc456abc",
	 		"expires_in": 1209600,
	 		"scope": "read",
	 		"token_type": "Bearer",
	 		"url": "https://pixometer.io/api/v1/users/4711/",
	 	 	"user_id": 4711
	 	}

		-   `access_token`:  access token
	    -   `expires_in`:    in seconds
	    -   `scope`:      `read`, `write` or `read write`

User Endpoint
-------------

With the exception of the auth token, the `users` endpoint models user 
description and permissions. *Anonymous* access to this endpoint only allows to 
register users, *Standard users* can see and edit their own user record, while 
*Admin users* can create, see, edit and delete any other user records.


### Method: GET ###

-   Endpoint: `users/`
-   Description: Get the list of users on this server. 
-   Input: -
-   Responses
    -   `200`: OK
    -   `401`: Unauthorized
    	-   `X-Pixolus-Error=10`: requesting user is inactive
    	-   `X-Pixolus-Error=11`: requesting user not logged in
-   Output:
        
        {
            "count": 1, 
            "next": null, 
            "previous": null, 
            "results": [
                {
                    "email": "the_current_user@email.org",
                    "is_active": True,
                    "url": "https://pixometer.io/api/v1/users/4711/",
                    "user_id": 4711
                }
            ]
        }

Notes:

-   `count`, `next`, `previous`: total number of users (visible to the logged in 
    user). We use pagination, i.e. on each returned page, you get (currently) 10 
    results. In order to get all results, we return `next` and `previous` URLs.
-   `url`: URL of the actual user record
-   `email`: email address / user name used for logging in
-   available filters: `created_after`, `created_before`, `last_login_after`, `last_login_before` 
-   available sorting: `date_joined`, `-date_joined` 

### Method: GET details of user ###

-   Endpoint: `users/x`
-   Description: Get details of a user x
-   Input: -
-   Responses
    -   `200`: OK
    -   `401`: Unauthorized
    	-   `X-Pixolus-Error=10`: requesting user is inactive
    	-   `X-Pixolus-Error=11`: requesting user not logged in
    -   `404`: if you try to get details of another user (not yourself) or a user that does not exist
-   Output:
        
        {
        	"email": "the_current_user@email.org",
            "is_active": True,
            "url": "https://pixometer.io/api/v1/users/4711/",
            "user_id": 4711
        }


### Method: POST ###

-   Endpoint: `users/`
-   Description: create a user. An email with an activation link is sent to the user.
-   Input: 
    -   `email`: required, string
    -   `password`: required, string, min_length: 8
-   Output:

		{
			"email": "foo@example.com",
 			"is_active": False,
 			"token": "273083df8464deded557d291ceb684b335c8d8b",
 			"url": "https://pixometer.io/api/v1/users/4711/",
 			"user_id": 4711
 		}
		
-   Responses
    -   `201`: Created
    -   `400`: if password was too short or email invalid
    	-   `X-Pixolus-Error=4`: password was too short
        -   `X-Pixolus-Error=5`: email address was invalid
    -   `409`: if a another account with this email address already exists 
    	-   `X-Pixolus-Error=6`: another account with this email address already exists
    
### Method: PATCH ###

-   Endpoint: `users/`
-   Description: (partially) update a user. For updating the e-mail address of 
    a user, you only have to provide the new email address. However, when updating 
    the password, you have to provide the `old_password`. 
    Note: A staff user can update a user's password without having to provide
    the `old_password`.
-   Input: 
    -   `email`: not required, string
    -   `password`: not required, string, min_length: 8
    -   `old_password`: not required, string, min_length: 8. Only required, if
        you want to update the password.
-   Output:

		{
			"email": "foo@example.com",
 			"is_active": False,
 			"url": "https://pixometer.io/api/v1/users/4711/",
            "user_id": 4711
 		}
 		
-   Responses
    -   `200`: OK
    -   `400`: if password was too short or email invalid
        -   `X-Pixolus-Error=3`: (old) password was incorrect
    	-   `X-Pixolus-Error=4`: password was too short
        -   `X-Pixolus-Error=5`: email address was invalid
        -   `X-Pixolus-Error=9`: (old) password is missing
    -   `401`: Unauthorized
    	-   `X-Pixolus-Error=10`: requesting user is inactive
    	-   `X-Pixolus-Error=11`: requesting user not logged in
    -   `404`: if you try to update another user (not yourself) or a user that does not exist
    
### Method: DELETE ###

-   Endpoint: `users/x`
-   Description: delete a user 
-   Input: -
-   Output: -
-   Responses
    -   `204`: User has been deleted
    -   `401`: Unauthorized
    	-   `X-Pixolus-Error=10`: requesting user is inactive
    	-   `X-Pixolus-Error=11`: requesting user not logged in
    -   `404`: if you try to delete another user (not yourself)
    

Password Reset
-------------

### Method: POST ###

-   Endpoint: `users/reset_password/`
-   Description: request an e-mail for resetting password
-   Input: 
    -   `email`: required, string
-   Output: - 
-   Responses
    -   `200`: OK
    -   `400`: 
    	-   `X-Pixolus-Error=2`: user with this e-mail address does not exist
        -   `X-Pixolus-Error=5`: email address is invalid
        -   `X-Pixolus-Error=8`: email address is missing
        -   `X-Pixolus-Error=10`: user with this e-mail address is not active


Meter Endpoint
--------------

### Method: GET ###

-   Endpoint: `meters/`
-   Description: get list of meters of authenticated user. 
-   Input: -
-   Responses
    -   `200`: OK
    -   `401`: Unauthorized
    	-   `X-Pixolus-Error=10`: requesting user is inactive
    	-   `X-Pixolus-Error=11`: requesting user not logged in
-   Output:
        
        {
            "count": 1, 
            "next": null, 
            "previous": null,
            "results": [
                {
                    "owner": "https://pixometer.io/api/v1/users/1/", , 
                    "url": "https://pixometer.io/api/v1/meters/1/",
                    "resource_id": 1, 
                    "changed_hash": "593e13f7831b0b06b78c1256401e8091", 
                    "created": "2014-11-06T17:13:08.296Z", 
                    "modified": "2014-11-06T17:13:08.296Z", 
                    "appearance": "mechanical_black", 
                    "fraction_digits": 1, 
                    "is_double_tariff": false, 
                    "location_in_building": "Basement", 
                    "meter_id": "124828341", 
                    "physical_medium": "electricity", 
                    "physical_unit": "kWh", 
                    "integer_digits": 6, 
                    "register_order": "-",
                    "city": "Berlin",
                    "zip_code": "10115",
                    "address": "Some Street 25",
                    "description": "Basement",
                    "label": "Power Meter Basement",
                    "resource_id": 1
                }
            ]
        }

Notes:

-   `count`: total number of returned meters that match the query 
-   `next`, `previous`: We use pagination, i.e. on each returned page, you get 
    (currently) 10 results. In order to get all results, we return `next` and 
    `previous` URLs.
-   `owner`: URL of the owner of this meter, may also be null
-   `url`: URL of this meter, providing the details of this meter
-   `created`, `modified`: timestamps generated on server
-   `appearance`,..., `register_order`: attributes of this meter
-   `resource_id`: the same ID as given in the URL
-   available filters: `owner`, `created_after`, `created_before`, 
    `modified_after`, `modified_before`, `physical_medium`, `location_in_building`, `meter_id` 
-   available sorting: `created`, `-created`, `owner`, `-owner`, `physical_medium`, `-physical_medium`,
    `location_in_building`, `-location_in_building`, `meter_id`, `-meter_id` 
-   The app pixometer shows the following information in the meter list:
    `location_in_building`
    ID: `meter_id` 
-   The app pixometer Pro shows the following information in the meter list:
    `label` (or `location_in_building` if `label` is empty)
    ID: `meter_id` `description`
    `zip_code` `city` `address` 

### Method: GET details of meter###

-   Endpoint: `meters/x`
-   Description: get details of a meter 
-   Input: -
-   Responses
    -   `200`: OK
    -   `401`: Unauthorized
    	-   `X-Pixolus-Error=10`: requesting user is inactive
    	-   `X-Pixolus-Error=11`: requesting user not logged in
    -   `404`: user is not allowed to access this meter
-   Output:
        
        {
			"owner": "https://pixometer.io/api/v1/users/1/", , 
            "url": "https://pixometer.io/api/v1/meters/1/", 
            "changed_hash": "593e13f7831b0b06b78c1256401e8091", 
            "created": "2014-11-06T17:13:08.296Z", 
            "modified": "2014-11-06T17:13:08.296Z", 
            "appearance": "mechanical_black", 
            "fraction_digits": 1, 
            "is_double_tariff": false, 
            "location_in_building": "Basement", 
            "meter_id": "124828341", 
            "physical_medium": "electricity", 
            "physical_unit": "kWh", 
            "integer_digits": 6, 
            "register_order": "-",
            "city": "Berlin",
            "zip_code": "10115",
            "address": "Some Street 25",
            "description": "Basement",
            "label": "Power Meter Basement",
            "resource_id": 1
         }

### Method: POST/PATCH ###

-   Endpoint: `meters/`
-   Description: create or update a meter
-   Input:
    -   `appearance`: required, string, max_length: 100, describes the 
        (technical) appearance of the meter. Currently, `mechanical_black` and
        `lcd` can be automatically read by the pixometer app
    -   `integer_digits`: required, int, the number of digits before comma that
        is displayed by this meter
    -   `fraction_digits`: required, int, the number of digits after comma that
        is displayed by this meter
    -   `location_in_building`: required, string, max_length: 500, describes the
        user-understandable location of the meter (i.e. `basement`, `kitchen`, 
        ...)
    -   `meter_id`: required, string, max_length: 100, describes the utility
        company's meter id
    -   `physical_medium`: required, string, max_length: 100, currently 
        supported are `electricity`, `gas` and `water`
    -   `physical_unit`: required, string, max_length: 100, currently supported
        are `kWh` (for `electricity`) and `m^3` (for `gas` and `water`)
    -   `is_double_tariff`: not required, bool
    -   `register_order`: not required, string, max_length: 100
    -   `city`: not required, string, max_length: 100
    -   `zip_code`: not required, string, max_length: 20
    -   `address`: not required, string, max_length: 100
    -   `description`: not required, string, max_length: 200
    -   `label`: not required, string, max_length: 100
-   Output:
        
        {
			"owner": "https://pixometer.io/api/v1/users/1/", , 
            "url": "https://pixometer.io/api/v1/meters/1/", 
            "changed_hash": "593e13f7831b0b06b78c1256401e8091", 
            "created": "2014-11-06T17:13:08.296Z", 
            "modified": "2014-11-06T17:13:08.296Z", 
            "appearance": "mechanical_black", 
            "fraction_digits": 1, 
            "is_double_tariff": false, 
            "location_in_building": "Basement", 
            "meter_id": "124828341", 
            "physical_medium": "electricity", 
            "physical_unit": "kWh", 
            "integer_digits": 6, 
            "register_order": "-",
            "city": "Berlin",
            "zip_code": "10115",
            "address": "Some Street 25",
            "description": "Basement",
            "label": "Power Meter Basement",
            "resource_id": 1
         }


-   Responses
    -   `201`: Created
    -   `401`: Unauthorized
    	-   `X-Pixolus-Error=10`: requesting user is inactive
    	-   `X-Pixolus-Error=11`: requesting user not logged in
    -   `404`: user is not allowed to access this meter
    
### Method: DELETE ###

-   Endpoint: `meters/x`
-   Description: delete a meter 
-   Input: -
-   Output: -
-   Responses
    -   `204`: meter has been deleted
    -   `401`: Unauthorized
    	-   `X-Pixolus-Error=10`: requesting user is inactive
    	-   `X-Pixolus-Error=11`: requesting user not logged in
    -   `404`: user is not allowed to access this meter

Reading Endpoint
----------------

### Method: GET ###

-   Endpoint: `readings/`
-   Description: get list of readings of authenticated user. 
-   Input: -
-   Responses
    -   `200`: OK
    -   `401`: Unauthorized
    	-   `X-Pixolus-Error=10`: requesting user is inactive
    	-   `X-Pixolus-Error=11`: requesting user not logged in
-   Output:
        
        {
            "count": 1, 
            "next": null, 
            "previous": null, 
            "results":
                {
                    "applying_person": "https://pixometer.io/api/v1/users/2/", 
                    "image_meta": {
                        "annotations": [
                            {
                                "rectangle": {
                                    "id": 3, 
                                    "annotation": 3, 
                                    "angle": -2.012509, 
                                    "center_x": 341, 
                                    "center_y": 663, 
                                    "height": 41, 
                                    "width": 302, 
                                    "sharpness": 1.137931, 
                                    "confidence": 0.948981
                                }, 
                                "id": 3, 
                                "meaning": "value", 
                                "text": "020741,9", 
                                "image": 4
                            }
                        ], 
                        "image": "https://pixometer.io/api/v1/images/4/", 
                        "image_download": "https://pixometer.io/api/v1/image/4/", 
                        "id": 4, 
                        "camera_model": "unknown", 
                        "flash": false, 
                        "frame_number": 24, 
                        "seconds_since_detection": 7.463481, 
                        "seconds_since_start": 12.2033, 
                        "lat": 0.0, 
                        "lng": 0.0, 
                        "os_version": "unknown OS", 
                        "pixolus_version": "unknown App version"
                    }, 
                    "image_second_tariff_meta": null, 
                    "meter": "https://pixometer.io/api/v1/meters/1/", 
                    "url": "https://pixometer.io/api/v1/readings/4/", 
                    "resource_id": 4,
                    "changed_hash": "515ee5404c3d5cf55903de7e5b3dec7d", 
                    "created": "2014-11-11T16:13:00.519Z", 
                    "modified": "2014-11-11T16:13:00.545Z", 
                    "applied_method": "ocr", 
                    "reading_date": "2014-11-11T16:12:49.626Z", 
                    "value": "20741.9", 
                    "value_second_tariff": "0", 
                    "provided_fraction_digits": 1, 
                    "provided_fraction_digits_second_tariff": 0,
                    "resource_id": 4
                }
        }

Notes:

-   `count`, `next`, `previous`: total number of readings, URLs to next and 
    previous results
-   `applying_person`: URL to user (owner) of this reading
-   `meter`: URL to corresponding meter
-   `url`: URL of this reading
-   `created`, `modified`: timestamps of creation and last modification time
-   `applied method`, `reading_date`, `value`, `value_second_tariff`, 
    `provided_fraction_digits`, `provided_fraction_digits_second_tariff`: 
    information about a reading
-   `image_meta`, `image_second_tariff_meta`: image and position of reading 
    detected by meter detection algorithm
    -   `annotations`: position of detected reading and OCR result
    -   `image`: URL to image endpoint with details about this image
    -   `image_download`: URL to image
    -   `id`: ID of this image
    -   other attributes: meta information
-   available filters: `value` (filters for readings with values greater than 
    the specified value), `applying_person`, `meter_ressource_id` (ressource ID of the meter, 
    not the meter_id (aka serial number)), `meter_id` (the serial number of the meter), `physical_medium`, `reading_date_after`,
    `reading_date_before`, `created_after`, `created_before`, `modified_after`, `modified_before`    
-   available sorting: `reading_date`, `-reading_date`, `created`, `-created`, `applying_person`, `-applying_person`
    
### Method: GET details of reading ###

-   Endpoint: `readings/x`
-   Description: get details of a reading 
-   Input: -
-   Responses
    -   `200`: OK
    -   `401`: Unauthorized
    	-   `X-Pixolus-Error=10`: requesting user is inactive
    	-   `X-Pixolus-Error=11`: requesting user not logged in
    -   `404`: user is not allowed to access this reading
-   Output:
        
        {
            "applying_person": "https://pixometer.io/api/v1/users/2/", 
            "image_meta": {
                "annotations": [
                    {
                        "rectangle": {
                            "id": 3, 
                            "annotation": 3, 
                            "angle": -2.012509, 
                            "center_x": 341, 
                            "center_y": 663, 
                            "height": 41, 
                            "width": 302, 
                            "sharpness": 1.137931, 
                            "confidence": 0.948981
                        }, 
                        "id": 3, 
                        "meaning": "value", 
                        "text": "020741,9", 
                        "image": 4
                    }
                ], 
                "image": "https://pixometer.io/api/v1/images/4/", 
                "image_download": "https://pixometer.io/api/v1/image/4/", 
                "id": 4, 
                "camera_model": "unknown", 
                "flash": false, 
                "frame_number": 24, 
                "seconds_since_detection": 7.463481, 
                "seconds_since_start": 12.2033, 
                "lat": 0.0, 
                "lng": 0.0, 
                "os_version": "unknown OS", 
                "pixolus_version": "unknown App version"
            }, 
            "image_second_tariff_meta": null, 
            "meter": "https://pixometer.io/api/v1/meters/1/", 
            "url": "https://pixometer.io/api/v1/readings/4/", 
            "changed_hash": "515ee5404c3d5cf55903de7e5b3dec7d", 
            "created": "2014-11-11T16:13:00.519Z", 
            "modified": "2014-11-11T16:13:00.545Z", 
            "applied_method": "ocr", 
            "reading_date": "2014-11-11T16:12:49.626Z", 
            "value": "20741.9", 
            "value_second_tariff": "0", 
            "provided_fraction_digits": 1, 
            "provided_fraction_digits_second_tariff": 0,
            "resource_id": 4
        }
        


### Method POST/PATCH ###

-   Endpoint: `readings/`
-   Description: create new reading or update attributes of a reading
-   Input: 
    -   `meter`: required, URL to associated meter
    -   `applied_method`: required, string, max_length:100
    -   `reading_date`: required, date of reading, datetime
    -   `value`: required, decimal, value of reading
    -   `value_second_tariff`: not required, decimal, value of second tariff if
        applicable
    -   `provided_fraction_digits`: not required, int, number of fraction digits
        found by our algorithm
    -   `provided_fraction_digits_second_tariff`: not required, int, number of 
        fraction digits of second tariff found by our algorithm
    -   `image_meta`, `image_second_tariff_meta`: not required, 
    	Please note that `image_meta` and `image_second_tariff_meta` may not be updated,
    	since the image is a proof for the reading.    	
        -   `image`: required, URL to 
            image details (image endpoint)
        -   `camera_model`: required, string, max_lenth: 100
        -   `flash`: bool, not required
        -   `frame_number`: integer, required
        -   `seconds_since_detection`, `seconds_since_start`: float, not 
            required
        -   `lat, long`: GPS coordinates, float, not required
        -   `os_version`: required, string, required; specifies the used OS 
            system and version, max_length: 100
        -   `pixolus_version`: required, string, max_length: 100
        -   list of `annotation` which depict position of reading and 
            reading value as detected by algorithm. not required
-   Output:
        
        {
            "applying_person": "https://pixometer.io/api/v1/users/2/", 
            "image_meta": {
                "annotations": [
                    {
                        "rectangle": {
                            "id": 3, 
                            "annotation": 3, 
                            "angle": -2.012509, 
                            "center_x": 341, 
                            "center_y": 663, 
                            "height": 41, 
                            "width": 302, 
                            "sharpness": 1.137931, 
                            "confidence": 0.948981
                        }, 
                        "id": 3, 
                        "meaning": "value", 
                        "text": "020741,9", 
                        "image": 4
                    }
                ], 
                "image": "https://pixometer.io/api/v1/images/4/", 
                "image_download": "https://pixometer.io/api/v1/image/4/", 
                "id": 4, 
                "camera_model": "unknown", 
                "flash": false, 
                "frame_number": 24, 
                "seconds_since_detection": 7.463481, 
                "seconds_since_start": 12.2033, 
                "lat": 0.0, 
                "lng": 0.0, 
                "os_version": "unknown OS", 
                "pixolus_version": "unknown App version"
            }, 
            "image_second_tariff_meta": null, 
            "meter": "https://pixometer.io/api/v1/meters/1/", 
            "url": "https://pixometer.io/api/v1/readings/4/", 
            "changed_hash": "515ee5404c3d5cf55903de7e5b3dec7d", 
            "created": "2014-11-11T16:13:00.519Z", 
            "modified": "2014-11-11T16:13:00.545Z", 
            "applied_method": "ocr", 
            "reading_date": "2014-11-11T16:12:49.626Z", 
            "value": "20741.9", 
            "value_second_tariff": "0", 
            "provided_fraction_digits": 1, 
            "provided_fraction_digits_second_tariff": 0,
            "resource_id": 4
        }
-   Responses
    -   `201`: Created
    -   `400`: if meter or image meta is missing
    -   `401`: Unauthorized
    	-   `X-Pixolus-Error=10`: requesting user is inactive
    	-   `X-Pixolus-Error=11`: requesting user not logged in

### Method: DELETE ###

-   Endpoint: `readings/x`
-   Description: delete a reading 
-   Input: -
-   Output: -
-   Responses
    -   `204`: eading has been deleted
    -   `401`: Unauthorized
    	-   `X-Pixolus-Error=10`: requesting user is inactive
    	-   `X-Pixolus-Error=11`: requesting user not logged in
    -   `404`: user is not allowed to access this reading

Image Endpoint
--------------

### Method: GET ###

-   Endpoint: `images/`
-   Description: list images
-   Input: -
-   Responses
    -   `200`: OK
    -   `401`: Unauthorized
    	-   `X-Pixolus-Error=10`: requesting user is inactive
    	-   `X-Pixolus-Error=11`: requesting user not logged in
-   Output:
        
        {
            "count": 1, 
            "next": null, 
            "previous": null, 
            "results": [
                {
                    "owner": "the_current_user@email.org", 
                    "image_download": "https://pixometer.io/api/v1/image/1/", 
                    "url": "https://pixometer.io/api/v1/images/1/", 
                    "created": "2014-11-07T10:00:23.925Z", 
                    "modified": "2014-11-07T10:00:23.942Z", 
                    "image": "image/FEDFF845-67B3-4A9C-9614-CCEE23407A61.jpg"
                }
            ]
        }
        
Notes:

-   available filters: `owner`, `created_after`, `created_before`, `modified_after`, `modified_before`  
-   available sorting: `created`, `-created`, `owner`, `-owner`
        
### Method: GET details of image ###

-   Endpoint: `images/x`
-   Description: get detail of an image
-   Input: -
-   Responses
    -   `200`: OK
    -   `401`: Unauthorized
    	-   `X-Pixolus-Error=10`: requesting user is inactive
    	-   `X-Pixolus-Error=11`: requesting user not logged in
    -   `404`: user is not allowed to access this image
-   Output:
        
        {
            "owner": "the_current_user@email.org", 
            "image_download": "https://pixometer.io/api/v1/image/1/", 
            "url": "https://pixometer.io/api/v1/images/1/", 
            "created": "2014-11-07T10:00:23.925Z", 
            "modified": "2014-11-07T10:00:23.942Z", 
            "image": "image/FEDFF845-67B3-4A9C-9614-CCEE23407A61.jpg",
            "resource_id": 1
        }
        

### Method: POST ###

-   Endpoint: `images/`
-   Description: create new image/update image
-   Input:
    -   `image`: required. HTTP body data must be sent as `multipart/form-data`
        with the appropriate content type, `binary` content transfer encoding, 
        and `form-data; name="image"; filename="<FILENAME>"` content 
        disposition.
-   Output:

		{
	        "owner": "the_current_user@email.org", 
	        "image_download": "https://pixometer.io/api/v1/image/1/", 
	        "url": "https://pixometer.io/api/v1/images/1/", 
	        "created": "2014-11-07T10:00:23.925Z", 
	        "modified": "2014-11-07T10:00:23.942Z", 
	        "image": "image/FEDFF845-67B3-4A9C-9614-CCEE23407A61.jpg",
            "resource_id": 1
	    }

-   Responses
    -   `201`: Created. Response contains JSON data for the created resource.
    -   `400`: if request was not valid 
    -   `401`: Unauthorized
    	-   `X-Pixolus-Error=10`: requesting user is inactive
    	-   `X-Pixolus-Error=11`: requesting user not logged in
-   Example for a valid HTTP request for posting image data (Note that the 
    `Content-Length` header does not give the byte count of the image data but 
    the byte count of the HTTP message body):

        POST /api/v1/images/ HTTP/1.1
        Host: pixometer.io
        Content-Type: multipart/form-data; boundary=1E0110DB-2D0B-4791-A84D-481B5BFCF2C8
        Content-Length: 6601
        Connection: keep-alive
        Accept: application/json
        User-Agent: pixometer_client
        Authorization: Token c112ba55fddb195f7174621e9d6146a675325874
        Accept-Language: de
        Accept-Encoding: gzip, deflate
        
        
        --1E0110DB-2D0B-4791-A84D-481B5BFCF2C8
        Content-Disposition: form-data; name="image"; filename="FEDFF845-67B3-4A9C-9614-CCEE23407A61.jpg"
        Content-Transfer-Encoding: binary
        Content-Type: image/jpeg
        
        [6369 bytes of raw binary image data]
        --1E0110DB-2D0B-4791-A84D-481B5BFCF2C8--
        
### Method: DELETE ###

Deletion of images is not allowed.

REST-API Endpoints for Reading Requests 
=======================================

The pixometer API also provides functionality for companies such as utilities and professional
meter readers. 


Employee Endpoint
-----------------

An employee is a normal user of the pixometer API and is associated with a
company. There are two types of employees: schedulers and meter readers. 

### Method: POST ###

-   Endpoint: `employees/`
-   Description: create a new pixometer user who is also an employee. Only schedulers and staff users can 
	create employees. Schedulers can only create employees of their own company and 
	the newly created employee receives an e-mail with an activation link.
-   Input:
    -   `email`: required, string
    -   `password`: required, string, min_length: 8
    -   `company`: required, URL of a company
    -   `is_scheduler`: required, boolean
-   Example:
	
        {
        	"email": "scheduler@example.org", 
        	"password": "foobarbaz", 
        	"employee": {
        		"company": {
                    "name": "SomeCompany"
                }, 
        		"is_scheduler": True
        		}
        }
-   Output:

		{
			"url": "https://pixometer.io/api/v1/users/1/", 
			"employee": {
				"company": {
                    "name": "SomeCompany"
                },
				"is_scheduler": True 
				}, 
			"is_active": True, 
			"email": "scheduler@example.org"
		}
	
-   Responses
    -   `201`: Created
    -   `400`: if password was too short or email invalid or employee information are missing
    	-   `X-Pixolus-Error=4`: password was too short
        -   `X-Pixolus-Error=5`: email address was invalid
    -   `409`: if a another account with this email address already exists 
    	-   `X-Pixolus-Error=6`: another account with this email address already exists


### Method: GET ###

-   Endpoint: `employees/`
-   Description: list employees. Schedulers can only see employees of the same company
-   Input: -
-   Responses
    -   `200`: OK
    -   `401`: Unauthorized
    	-   `X-Pixolus-Error=10`: requesting user is inactive
    	-   `X-Pixolus-Error=11`: requesting user not logged in
-   Output:
        
        {
            "count": 1, 
            "next": null, 
            "previous": null, 
            "results": [
                {
					"url": "https://pixometer.io/api/v1/users/1/",
					"email": "reader@example.org",
					"employee": {
						"id": 1,
						"company": {
                            "name": "SomeCompany"
                        },
						"is_scheduler": false
					},
					"is_active": true
				}
            ]
        }

### Method: GET details of employee ###

-   Endpoint: `employees/x`
-   Description: get detail of an employee. Employees may see their own information. 
	Schedulers may see information about employees of their own company.
-   Input: -
-   Responses
    -   `200`: OK
    -   `401`: Unauthorized
    	-   `X-Pixolus-Error=10`: requesting user is inactive
    	-   `X-Pixolus-Error=11`: requesting user not logged in
    -   `404`: user is not allowed to access this employee
-   Output:
        
        {
			"url": "https://pixometer.io/api/v1/users/1/",
			"email": "scheduler@example.org",
			"employee": {
				"id": 1, // employee ID
				"company": {
                    "name": "SomeCompany"
                },
				"is_scheduler": true
			},
			"is_active": true
		}

Reading Request Endpoint
------------------------

A scheduler creates a request that a meter reader (=delegate) should read a meter. 
The scheduler must be the owner of the meter. As long as there exists an open reading request
for a meter, the meter reader (who should read the meter=delegate) can "see" the meter and 
create a reading. 
The scheduler can filter the list of reading requests to get already fulfilled requests 
(i.e. the meter reader has created a reading for the meter) or reading requests that are
still open.

### Method: POST ###

-   Endpoint: `reading_requests/`
-   Description: create a new reading request for a meter. Only schedulers may create 
	reading requests.
-   Input:
    -   `delegate`: required, string, email address of the meter reader who should read the 
    	meter 
    -   `meter`: required, URL of the meter to be read
    -   `planned_reading_date`: start of the planned reading interval
	-   `planned_reading_date_end`: end of the planned reading interval
    -   `min_expected_value`: minimum expected value of the first counter; for plausibility checks
    -   `max_expected_value`: maximum expected value of the first counter; for plausibility checks
    -   `min_expected_value_second_tariff`: minimum expected value of the second counter; 
    	for plausibility checks
    -   `max_expected_value_second_tariff`: maximum expected value of the second counter; 
    	for plausibility checks
-   Example:
	
        {
			"delegate": "reader@example.org",
			"meter": "https://pixometer.io/api/v1/meters/1/",
			"planned_reading_date": "2015-12-01T00:00Z",
			"planned_reading_date_end": "2015-12-31T00:00Z",
			"min_expected_value": 123456,
			"max_expected_value": 123654,
			"min_expected_value_second_tariff": null,
			"max_expected_value_second_tariff": null
		}
	
-   Output:

		{
			"url": "https://pixometer.io/api/v1/reading_requests/1/",
			"reading": null,
			"owner": "scheduler@example.org",
			"delegate": "reader@example.org",
			"meter": "https://pixometer.io/api/v1/meters/1/",
			"changed_hash": "1233244cd2464321",
			"created": "2015-10-06T11:56:45.554Z",
			"modified": "2015-10-06T11:56:45.555Z",
			"planned_reading_date": "2015-12-01T00:00:00Z",
			"planned_reading_date_end": "2015-12-31T00:00:00Z",
			"min_expected_value": "123456",
			"max_expected_value": "123654",
			"min_expected_value_second_tariff": null,
			"max_expected_value_second_tariff": null
		}
	
-   Responses
    -   `201`: Created
    -   `401`: if the user is not logged in
    -   `403`: if the user is not allowed to create this reading request
    -   `409`: if a reading request for the same combination meter/delegate/planned_reading_date 
    	already exists
    	
### Method: GET ###

-   Endpoint: `reading_requests/`
-   Description: list reading requests. Schedulers may see all reading requests of their
	company. A meter reader may see only reading requests where he is the delegate.
-   Input: -
-   Responses
    -   `200`: OK
    -   `401`: Unauthorized
    	-   `X-Pixolus-Error=10`: requesting user is inactive
    	-   `X-Pixolus-Error=11`: requesting user not logged in
-   Output:
        
        {
			"count": 1,
			"next": null,
			"previous": null,
			"results": [
				{
					"url": "https://pixometer.io/api/v1/reading_requests/1/",
					"reading": null,
					"owner": "scheduler@example.org",
					"delegate": "reader@example.org",
					"meter": "https://pixometer.io/api/v1/meters/1/",
					"changed_hash": "274954654abc4645",
					"created": "2015-10-06T11:56:45.554Z",
					"modified": "2015-10-06T11:56:45.555Z",
					"planned_reading_date": "2015-12-01T00:00:00Z",
					"planned_reading_date_end": "2015-12-31T00:00:00Z",
					"min_expected_value": "123456",
					"max_expected_value": "123654",
					"min_expected_value_second_tariff": null,
					"max_expected_value_second_tariff": null
				}
			]
		}
Notes:

-   available filters: 
	- `open`: if True: returns reading request that are still open (i.e. a reading is not associated with this reading request)				
-   available sorting: `created`, `-created`, `delegate`, `-delegate`, `planned_reading_date`, `-planned_reading_date`

### Method: GET details of a reading request ###

-   Endpoint: `reading_requests/x`
-   Description: get detail of a reading request. 
-   Input: -
-   Responses
    -   `200`: OK
    -   `401`: Unauthorized
    	-   `X-Pixolus-Error=10`: requesting user is inactive
    	-   `X-Pixolus-Error=11`: requesting user not logged in
    -   `403`: user is not allowed to access this reading request
    -   `404`: user is not allowed to access this reading request
-   Output:
        
	{
		"url": "https://pixometer.io/api/v1/reading_requests/1/",
		"reading": null, 
		"owner": "scheduler@example.org",
		"delegate": "reader@example.org",
		"meter": "https://pixometer.io/api/v1/meters/1/",
		"changed_hash": "27497629ba3146547a",
		"created": "2015-10-06T11:56:45.554Z",
		"modified": "2015-10-06T11:56:45.555Z",
		"planned_reading_date": "2015-12-01T00:00:00Z",
		"planned_reading_date_end": "2015-12-31T00:00:00Z",
		"min_expected_value": "123456",
		"max_expected_value": "123654",
		"min_expected_value_second_tariff": null,
		"max_expected_value_second_tariff": null
	}
		


